import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set, onValue, push, query, orderByChild, child, get} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';

const firebaseConfig = {
  apiKey: "AIzaSyCIrBgyhPa4TG88dw4EeubIyvb6dUBge9s",
  authDomain: "etrr-online.firebaseapp.com",
  databaseURL: "https://etrr-online-default-rtdb.firebaseio.com",
  projectId: "etrr-online",
  storageBucket: "etrr-online.appspot.com",
  messagingSenderId: "449436731464",
  appId: "1:449436731464:web:a638ea84fbbea7221df07d"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();

const user = auth.currentUser;

const database = getDatabase();

var nombre;


var mensajeRef = document.getElementById("textoChatId");
var chatRef = document.getElementById("mensajeChatId");

// Referencia al DOM, botón de enviar mensaje.
var enviarRef = document.getElementById("buttonChatId");
enviarRef.addEventListener("click", enviarMensaje);

// Referencia al DOM, botón de LogOut.
var logOut = document.getElementById("logOutButton");
logOut.addEventListener("click", loggingOut);

let uid;
let correo;

onAuthStateChanged(auth, (user) => {
    if (user) {
      // UID y Correo del usuario logueado
      uid = user.uid;
      correo = user.email;
      console.log("Usuario es:" + " " + correo + "UID: " + uid);

    } else {
    }
});


function enviarMensaje(){
    let mensaje = mensajeRef.value;

    // Pusheamos a la base de datos un nuevo mensaje del foro.
    push(ref(database, "mensaje/"), {
        msg: mensaje,
        email: correo 
    })

    mensajeRef.value = "";
    console.log("Mensaje enviado. Correo: " + correo);
}

const queryMensajes = query(ref(database, 'mensaje/'), orderByChild('email'));
console.log(queryMensajes);

// ref(database, 'mensaje/-N5kqbIVFkoEnSPxiE12')

onValue(queryMensajes, (snapshot) => {
    // const lectura = snapshot.val();

    // chatRef.innerHTML += `
    //     <p>${lectura.email}: ${lectura.msg}</p>
    // `;
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val().msg;
      console.log(childData);
      
      chatRef.innerHTML += `
          <p>${childSnapshot.val().email}: ${childSnapshot.val().msg}</p>
      `;
    });
});

function loggingOut(){

  const auth = getAuth();
signOut(auth).then(() => {
  window.location.href ="../index.html";
}).catch((error) => {
});

}