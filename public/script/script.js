import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';

const firebaseConfig = {
  apiKey: "AIzaSyCIrBgyhPa4TG88dw4EeubIyvb6dUBge9s",
  authDomain: "etrr-online.firebaseapp.com",
  databaseURL: "https://etrr-online-default-rtdb.firebaseio.com",
  projectId: "etrr-online",
  storageBucket: "etrr-online.appspot.com",
  messagingSenderId: "449436731464",
  appId: "1:449436731464:web:a638ea84fbbea7221df07d"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase();

// Referencias al DOM - Buttons
var ingresoRef = document.getElementById("logInButtonId");
var registroRef = document.getElementById("buttonSignInId");

ingresoRef.addEventListener("click", logInUser);
registroRef.addEventListener("click", createUser);

// Referencias al DOM - LogIn
var correoLogIn = document.getElementById("inputEmailLogin");
var contraLogIn = document.getElementById("inputPasswordLogIn");

// Referencias al DOM - SignIn
var correoSignIn = document.getElementById("emailId");
var ciudadRef = document.getElementById("cityId");
var nombreRef = document.getElementById("nameId");
var apellidoRef = document.getElementById("lastNameId");
var contraSignIn = document.getElementById("inputPassword");

function createUser(){

  console.log("Ingreso a la función createUser");

  let correo = correoSignIn.value;
  let pass = contraSignIn.value;
      
  if((correo != '') && (pass!= '')){
    createUserWithEmailAndPassword(auth, correo, pass)
      .then((userCredential) => {                   
        const user = userCredential.user;
        const uid = user.uid; 
        console.log("Usuario: " + user + " ID:" + uid);
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
    });

    console.log("Usuario creado");

    set(ref(database, 'usuarios/' + nombreRef.value + ' ' + apellidoRef.value),{
      email: correo,
      nombre: nombreRef.value,
      apellido: apellidoRef.value,
      ciudad: ciudadRef.value
    })

  }else{
      alert("Ingresar usuario y contraseña");
  }
}

function logInUser(){

  let correo = correoLogIn.value;
  let pass = contraLogIn.value;

  signInWithEmailAndPassword(auth, correo, pass)
  .then((userCredential) => {
    const user = userCredential.user;
    window.location.href ="./edit.html";
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
  });

}


